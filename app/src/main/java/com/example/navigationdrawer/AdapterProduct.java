package com.example.navigationdrawer;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.MyView> {
    Context context;
    List<ModelProduct> data;


    public AdapterProduct(Context context, List<ModelProduct> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterProduct.MyView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_product, parent, false);
        return new MyView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterProduct.MyView holder, final int position) {
        holder.nama.setText(data.get(position).getNama());
        Picasso.get().load(data.get(position).getImage()).into(holder.photo);

        holder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String foto = data.get(position).getImage();
                String nama =  data.get(position).getNama();
                String harga = data.get(position).getHarga();
                String deskripsi = data.get(position).getDeskripsi();
                Intent intent = new Intent(context, DetailProduct.class);
                intent.putExtra("foto", foto);
                intent.putExtra("nama", nama);
                intent.putExtra("harga", harga);
                intent.putExtra("deskripsi", deskripsi);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyView extends RecyclerView.ViewHolder {
        ImageView photo;
        TextView nama, detail;
        public MyView(@NonNull View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.photo_product);
            nama = itemView.findViewById(R.id.nama_product);
            detail = itemView.findViewById(R.id.shop);
        }
    }
}
