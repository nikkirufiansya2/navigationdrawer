package com.example.navigationdrawer;

public class UrlApi {

    public static String BASE_URL = "https://masak-apa.tomorisakura.vercel.app";

    public static Api getApi() {
        return RetrofitClient.getClient(BASE_URL).create(Api.class);
    }
}
