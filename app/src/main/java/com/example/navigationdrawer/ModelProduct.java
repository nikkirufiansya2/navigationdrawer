package com.example.navigationdrawer;

public class ModelProduct {
    private String image, nama;
    private String deskripsi, harga;

    public ModelProduct(String image, String nama, String deskripsi, String harga){
        this.image = image;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.harga = harga;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }



    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
