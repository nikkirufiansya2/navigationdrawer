package com.example.navigationdrawer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailProduct extends AppCompatActivity {
    ImageView photo;
    TextView nama, harga, deskripsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);
        photo = findViewById(R.id.foto);
        nama = findViewById(R.id.nama);
        harga = findViewById(R.id.harga);
        deskripsi = findViewById(R.id.deskripsi);
        String foto_product = getIntent().getStringExtra("foto");
        String nama_product = getIntent().getStringExtra("nama");
        String harga_product = getIntent().getStringExtra("harga");
        String deskripsi_product = getIntent().getStringExtra("deskripsi");
        Picasso.get().load(foto_product).into(photo);
        nama.setText(nama_product);
        harga.setText(harga_product);
        deskripsi.setText(deskripsi_product);
    }
}