package com.example.navigationdrawer;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {
    @GET("/api/recipes")
    Call<ResponseBody> getListMakanan();
}
